Proyecto 1: generador de graficos en base a archivos pdb
 Este proyecto consiste en un script bash el cual tiene por funcion prinsipal generar un grafo de las cadenas de una proteina, 
 mediante la informacion localizada en el apartado "ATOM" del archivo pdb de la proteina blanco.
 Para esto el usuario podra:
 
*  ingresar el codigo de 4 letras de la proteina blanco.

 Para el correcto funcionamiento de este programa se recomienda como requisito previo:

*  bash shell.
*  libreria Graphviz
*  extraer el archivo non_protein.txt de non_protein.bz2
*  conservar los archivos de este pp1.sh, progressbar.sh y non_protein.txt en la misma carpeta contenedora para evitar fallos en la ejecucion del script.
*  Linux OS en cualquiera de sus distribuciones.

Para la ejecucion de este programa se debe:

*  Posicionarse en la carpeta donde se encuentra los archivos descargados
*  Ingresar en la consola de comandos de Linux OS.
*  Ingresar la siguiente linea de comando:
    bash pp1.sh proteina
*  donde "proteina" representa el nombre de la proteina blanco

Este programa se desarrollo en:

*  gedit(editor de texto)


 Este programa se desarrollo con leguaje:

*  bash
*  awk


Este programa fue desarrollo por:

*  Nicolas Avila.

agradecimiento a Teddy Skarin autor del script progressbar.sh. culla solucion fue implementada en este trabajo.

NOTA: la generacion del grafo tarda en efectuarse

NOTA2: la imagen del grafo es defectuosa, se esta trabajando en su mejora