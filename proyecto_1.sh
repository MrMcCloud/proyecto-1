#!/bin/bash
source progressbar.sh

clear
if [[ -f "error_wget.txt" ]]; then 
	rm -f error_wget.txt
fi

if [[ "$#" == "0" ]]; then
    echo " `date +%d%m%y` Error: no se ha ingresado parametro" >> error_par.txt
	echo "Error: no se ha ingresado parametro"
    exit 1 
else
	if [[ "$#" == "1" ]]; then
		if [[ ! -f "non_protein2.txt" ]]; then 
			awk -F"," '{ gsub(/\"/,""); print $1 }' non_protein.txt > non_protein2.txt
		fi

		for i in `cat non_protein2.txt`; do
		    if [[ "$i" == "$@" ]]; then
				echo " `date +%d%m%y` Error: el parametro ingresado no es una proteina" >> error_par.txt
				echo "Error: el parametro ingresado no es una proteina"
				exit 1
		    fi
		done

		wget https://files.rcsb.org/download/$@.pdb $PWD 1> error_wget.txt
	else 
		echo "`date +%d%m%y` Error: se ingreso más de un parametro" >> error_par.txt
		echo "Error: se ingreso más de un parametro"
		exit 1
	fi
fi

awk -F" " '$1=="ATOM" { print $1, " ", $2, " ", $3, " ", $4, " ", $5, " ", $6 }' $@.pdb > ATOM_protein.txt

echo -e "digraph esctructure {\n\trankdir = LR;" > map.dot
echo -e "\tsize="1700";">> map.dot
echo -e "\toverlap=prisma;" >> map.dot
echo -e "\nposesando informacion:"

oldIFS=$IFS 
IFS=$'\n'
chain="null"
position="null"
residuo="null"
element="null"
count="0"
large=`cat ATOM_protein.txt | wc -l`
for j in `cat ATOM_protein.txt`; do
	char=`echo $j | awk -F" " '{ print $5 }'`
	num=`echo $j | awk -F" " '{ print $6 }'`
	aa=`echo $j | awk -F" " '{ print $4 }'`
	element=`echo $j | awk -F" " '{ print $3 }'`
	
	if [[ "$chain" != "$char" ]]; then
		if [[ "$chain" != "null" ]]; then
			cat caraa.txt >> map.dot
			cat linkaa.txt >> map.dot
			echo " " >> map.dot
			cat car.txt >> map.dot
			cat link.txt >> map.dot
			echo -e "\t}" >> map.dot
		fi

		chain=$char
		position=$num
		residuo=$aa
		name_residuo="CAD$chain""_""$residuo"
		name="$position $residuo$element"
		
		echo -e "\tsubgraph $chain {\n\t\tlabel="chain $chain";" >> map.dot
		echo -e "\t\tnode [shape=box];" > linkaa.txt
		echo -e "\t\tnode [shape=circle];" > link.txt
		echo -e "\t\t$name [label = "$element"];" > car.txt
		rm -f caraa.txt
		touch caraa.txt
		echo -e "\t\t$name -> $name_residuo;" >> link.txt
		continue
	fi

	if [[ "$position" != "$num" ]]; then
		position=$num
		name_aa="CAD$chain""_""$aa"
		name_residuo="CAD$chain""_""$residuo"
		echo -e "\t\t$name_aa [label="$aa"];" >> caraa.txt
		echo -e "\t\t$name_residuo -> $name_aa;" >> linkaa.txt

		residuo=$aa
		name_residuo="CAD$chain""_""$residuo"
		name="$position $residuo$element"

		echo -e "\t\t$name [label="$element"];" >> car.txt
		echo -e "\t\t$name -> $name_residuo;" >> link.txt

	elif [[ "$residuo" == "$aa" ]]; then
		residuo=$aa
		name_residuo="CAD$chain""_""$residuo"
		name="$position $residuo$element"

		echo -e "\t\t$name [label="$element"];" >> car.txt
		echo -e "\t\t$name -> $name_residuo;" >> link.txt
	fi

	count=$(( $count + 1 ))
	ProgressBar $count $large
done
IFS=$old_IFS

cat caraa.txt >> map.dot
cat linkaa.txt >> map.dot
echo " " >> map.dot
cat car.txt >> map.dot
cat link.txt >> map.dot
echo -e "\t}" >> map.dot
echo "}" >> map.dot
echo " "

dot -Tjpg -o estructure.jpg map.dot